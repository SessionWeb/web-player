var HLS = new Hls();
var video, source, requests, stats, qlty;

$(function(){
  /****    CONTAINERS    ****/
  video     =  document.getElementById('video'); // attachMedia does not work with jQuery
  qlty      =  $('#quality');
  stats     =  $('#video-info');
  source    =  $('#source');
  requests  =  $('#requests div');
  supported =  $('#browserSupport');

  if(Hls.isSupported() === false){
    supported.find('.modal-title').text('Invalid Browser');
    supported.find('.modal-body').html('<p>Your browser does not support HLS video playback. Please update your browser.</p>');
    supported.modal();
  }
  
  source.submit(function(e){
    var stream = this.streamURL.value
    if(stream == '')
      stream = this.videoSelect.value;
    
    streaming(stream);
          
    e.preventDefault();
    return false;
  });

  requests.siblings('button[name="clear"]').on('click', function(){ requests.html('') });
  
  HLS.on(Hls.Events.FRAG_BUFFERED, function(event, data){
    rqst(data.frag.url);
  });
  
  HLS.on(Hls.Events.MANIFEST_LOADING, function(event, data){
    rqst(data.url);
  });
  
  HLS.on(Hls.Events.LEVEL_SWITCHING, function(event, data){
    rqst(data.url[0]);
  });
  
  HLS.on(Hls.Events.SUBTITLE_TRACK_LOADING, function(event, data){
    rqst(data.url);
  });
  
  /****    PMD    ****/
  source = $('#pmd-source');

  source.submit(function(e){
    var pmdvideo = document.getElementById("pmd-video");
    var stream = this.streamURL.value
    if(stream == '')
      stream = this.videoSelect.value;
    
    pmdvideo.getElementsByTagName('source')[0].src = stream;
    pmdvideo.load();
    pmdvideo.play();
          
    e.preventDefault();
    return false;
  });

  function rqst(url){
    requests.append(document.createTextNode(url));
    requests.append('<br>');  
  }

  function qualityButtons(){
    var levels = HLS.levelController.levels;

    qlty.find('button').remove();
    
    $.each(levels, function(i){
      $('<button/>', {
        class: 'btn btn-default',
        text: this.bitrate,
        click: function(){
          setLevel(i);
        }
      }).appendTo(qlty);
    });
  }

  function setLevel(level){
    HLS.currentLevel = level;
    statsUpdate(level);
  }
  
  function statsUpdate(levelID){
    var level = HLS.levelController.levels[levelID];
    
    var parameters = stats.children('span');
    $.each(parameters, function(){
      var param = $(this).attr('class');
      $(this).html(level[param]);
    });
  }

  var streaming = function(url){
    requests.siblings('button[name="clear"]').trigger('click');
    
    HLS.loadSource(url);
    HLS.attachMedia(video);
    
    HLS.on(Hls.Events.MANIFEST_PARSED, function(){
      video.play();
      qualityButtons();
      statsUpdate(HLS.levelController.firstLevel);
    });
  }
});